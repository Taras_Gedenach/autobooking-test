import React from 'react';
import FetchedSelect from '../FetchedSelect';

const BrandSelector = ({pathValue}) => (
    <FetchedSelect
        caption = "Бренди"
        param = "brand_slug"
        pathValue = {pathValue}
        source = "https://beta.autobooking.com/api/test/v1/search/brands_terms"
    />
);

export default BrandSelector;