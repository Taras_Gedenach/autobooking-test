import React, { useEffect, useState } from "react";
import ServiceSelector from "../ServiceSelector";
import BrandSelector from "../BrandSelector";
import StyleSelector from "../StyleSelector";
import { useLocation } from "react-router-dom";
import Page, { Grid, GridColumn } from '@atlaskit/page';
import { getParseLinkURLFromPath } from "../pathUtils";

const ServicesPage = () => {
    const {pathname} = useLocation();
    const [requestedParams, setRequestedParams] = useState({});

    const userOrderURL = getParseLinkURLFromPath(pathname);

    useEffect(() => {
        fetch(userOrderURL)
            .then(data => data.json())
            .then((data) => {
                setRequestedParams(data);
            })
    }, []);
    
    const {service, brand, style} = requestedParams;
    
    return (
        <Page>
            <Grid spacing="comfortable">
                <GridColumn medium = {3}>
                    <ServiceSelector pathValue = {service}/>
                </GridColumn>
                <GridColumn medium = {3}>
                    <BrandSelector pathValue = {brand}/>
                </GridColumn>
                <GridColumn medium = {3}>
                    <StyleSelector pathValue = {style}/>
                </GridColumn>
            </Grid>
        </Page>
    );
};

export default ServicesPage;