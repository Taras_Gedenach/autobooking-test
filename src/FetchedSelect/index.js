import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { useHistory, useLocation } from 'react-router-dom';
import { createURL, parsePath } from '../pathUtils';
import Spinner from '@atlaskit/spinner';

const CustomSelect = styled.select`
    max-width: 100%;
`;

const FetchedSelect  = ({caption, param, source, pathValue}) => {
    const [selectedValue, updateValue] = useState("");
    const [optionsList, setOptionsList] = useState(null);
    const isLoading = !optionsList;
    
    let history = useHistory();
    let {pathname} = useLocation();
    
    const pathParams = parsePath(pathname);
    
    const onSelect = (e) => {
        const value = e.target.value;
        let paramValue = value;

        if (value === "default") paramValue = null;

        const targetUrl = createURL(param, paramValue, pathParams);
        updateValue(value);
        history.push(targetUrl);
    }
    
    useEffect(() => {
        fetch(source)
            .then(data => data.json())
            .then((data) => {
                setOptionsList(data.data);
            })
            .catch(error => {
                this.props.history.push("/404");
            });
    }, []);
    
    const selectedOption = selectedValue || pathValue?.slug;

    return (
        <form>
            <div>
                {caption}:
                {isLoading && <Spinner />}
            </div>
            <CustomSelect
                onChange = {onSelect}
                value = {selectedOption}
            >
                <option value = "default">
                    Оберіть
                </option>
                {isLoading && pathValue?.label && 
                    <option key = {pathValue.id} value = {pathValue.slug}>
                        {pathValue.label}
                    </option>
                }
                {!isLoading && optionsList.map(option =>
                    <option key = {option.id} value = {option.slug}>
                        {option.label}
                    </option>
                )}
            </CustomSelect>
        </form>
    );
};

export default FetchedSelect;