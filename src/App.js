import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import ServicesPage from './ServicesPage';

function App() {
    return (
        <Router>
            <Switch>
                <Route path = "*">
                    <ServicesPage />
                </Route>
                <Route exact path = "/404/">
                    <h1>404 - Not found</h1>
                </Route>
                <Redirect to = "/404/" />
            </Switch>
        </Router>
    );
}

export default App;
