export function createURL(paramToUpdate, value, oldUrlParams) {
    const paramsKeys = ["service_slug", "brand_slug", "style_slug"];
    const prefixes = ['s-', 'b-', 'st-'];
    let url = "/";
    
    for (let x in paramsKeys) {
        const paramKey = paramsKeys[x];
        let temp = prefixes[x];

        if (paramKey === paramToUpdate) {
            if (value === null) continue;
            temp += value;
            url += temp;
        } else {
            if (!oldUrlParams[paramKey]) continue;
            temp += oldUrlParams[paramKey];
            url += temp;
        }
        url += "/";
    }
    return url;
}
/**
 * Parses pathname and returns named parameters
 * @param {string} pathname - pathname from current location
 * @returns {object} {service_slug: string, brand_slug: string, style_slug: string}
 */
export function parsePath (pathname) {
    const serviceSlug = pathname.match(/s-([a-zA-Z-]+)?/);
    const brandSlug = pathname.match(/b-([a-zA-Z-]+)?/);
    const styleSlug = pathname.match(/st-([a-zA-Z-]+)?/);
    //Matched value or null would be assigned
    return {
        service_slug: serviceSlug &&  serviceSlug[1],
        brand_slug: brandSlug &&  brandSlug[1],
        style_slug: styleSlug &&  styleSlug[1]
    };
}
export const apiURL = "https://beta.autobooking.com/api";

export const getParseLinkParamsFromPath = pathname => {
    const pathParams = parsePath(pathname);
    return Object.keys(pathParams)
        .filter(
            key => pathParams[key] !== null
        ).map(
            key => `${key}=${pathParams[key]}`
        ).join("&");
}
    

export const getParseLinkURLFromPath = pathname =>
    `${apiURL}/test/v1/search/parse_link?${getParseLinkParamsFromPath(pathname)}`;
