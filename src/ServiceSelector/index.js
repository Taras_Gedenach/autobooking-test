import React from 'react';
import FetchedSelect from '../FetchedSelect';

const ServiceSelector = ({pathValue}) => (
    <FetchedSelect
        caption = "Сервіси"
        param = "service_slug"
        pathValue = {pathValue}
        source = "https://beta.autobooking.com/api/test/v1/search/terms"
    />
);

export default ServiceSelector;