import React from 'react';
import FetchedSelect from '../FetchedSelect';

const StyleSelector = ({pathValue}) => (
    <FetchedSelect
        caption = "Стилі"
        param = "style_slug"
        pathValue = {pathValue}
        source = "https://beta.autobooking.com/api/test/v1/search/styles"
    />
);

export default StyleSelector;